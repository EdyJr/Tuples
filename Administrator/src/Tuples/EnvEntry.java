package Tuples;

import net.jini.core.entry.*;

public class EnvEntry implements Entry {

    public String name;

    public EnvEntry() {}

    public EnvEntry(String name) {
        this.name = name;
    }

    public String toString() {
        return "Environment's name: " + name;
    }
}