import Tuples.DevEntry;
import Tuples.EnvEntry;
import Tuples.UsrEntry;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;
import net.jini.core.lease.Lease;
import net.jini.core.transaction.TransactionException;
import net.jini.space.JavaSpace;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class WindowController {

    @FXML TableColumn usrColumn;
    @FXML TableColumn envColumn;
    @FXML TableColumn devColumn;

    @FXML TableView<TableElement> usrTable;
    @FXML TableView<TableElement> envTable;
    @FXML TableView<TableElement> devTable;

    private ObservableList<TableElement> envList = FXCollections.observableArrayList();
    private ObservableList<TableElement> devList = FXCollections.observableArrayList();
    private ObservableList<TableElement> usrList = FXCollections.observableArrayList();

    private TableElement selectedEnv;
    private TableElement selectedDev;

    private JavaSpace space;

    private final long timeToRead = 1 * 100;

    public void setSpace(JavaSpace space) {
        this.space = space;
    }

    public void initUI() {

        envTable.setItems(envList);
        Label label = new Label("No environments created. Click the + button to create one.");
        envTable.setPlaceholder(label);
        envColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<TableElement, String>,
                ObservableValue<String>>) c -> c.getValue().getName());
        envTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selectedEnv = newValue;
                showDevs();
                showUsr();
            }
        });

        devTable.setItems(devList);
        devTable.setPlaceholder(new Label("No devices added.\nClick the + button to create one."));
        devColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<TableElement, String>,
                ObservableValue<String>>) c -> c.getValue().getName());
        devTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null)
                selectedDev = newValue;
        });

        usrTable.setItems(usrList);
        usrTable.setPlaceholder(new Label("No users connected"));
        usrColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<TableElement, String>,
                ObservableValue<String>>) c -> c.getValue().getName());
        showEnvs();
    }

    //Environment methods

    private List<EnvEntry> getEnvs() {

        List<EnvEntry> envs = new ArrayList<>();
        EnvEntry template = new EnvEntry();
        while (true) {
            EnvEntry gotEntry;
            try {
                gotEntry = (EnvEntry) space.take(template, null, 1 * 100);
                if (gotEntry == null)
                    break;
                envs.add(gotEntry);
            } catch (Exception e) {
                System.out.println(e);
                break;
            }
        }
        for (EnvEntry entry : envs) {
            try {
                space.write(entry, null, Lease.FOREVER);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return envs;
    }

    public void showEnvs() {

        List<EnvEntry> envs = getEnvs();
        envList.clear();
        for (EnvEntry entry : envs) {
            try {
                envList.add(new TableElement(entry.name));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int getGreatestEnvId() {

        int greatest = 0;
        List<EnvEntry> envs = getEnvs();
        for (EnvEntry entry : envs) {
            String stringId = entry.name.substring("Env".length());
            int id = Integer.parseInt(stringId);
            greatest = Integer.max(greatest, id);
        }
        return greatest;
    }

    @FXML
    public void addEnv() {

        String envName = "Env" + (getGreatestEnvId() + 1);
        EnvEntry envEntry = new EnvEntry(envName);
        try {
            space.write(envEntry, null, Lease.FOREVER);
            envList.add(new TableElement(envEntry.name));
        } catch (TransactionException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void deleteEnv() {

        if (selectedEnv == null) {
            showWarning("Select an environment");
            return;
        }
        String envName = selectedEnv.getName().get();
        int index = envList.indexOf(selectedEnv);
        DevEntry devTemp = new DevEntry(null, envName);
        UsrEntry usrTemp = new UsrEntry(null, envName);
        try {
            DevEntry devGot = (DevEntry) space.read(devTemp, null, timeToRead);
            if (devGot != null) {
                showWarning("Not an empty environment");
                showDevs();
                return;
            }
            UsrEntry usrGot = (UsrEntry) space.read(usrTemp, null, timeToRead);
            if (usrGot != null) {
                showWarning("Not an empty environment");
                showUsr();
                return;
            }
            EnvEntry template = new EnvEntry(envName);
            space.take(template, null, Long.MAX_VALUE);
            envList.remove(index);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (envList.isEmpty()) {
            selectedEnv = null;
            devColumn.setText("Devices");
            usrColumn.setText("Users");
            usrList.clear();
            devList.clear();
        }
    }

    //Device methods

    public void showDevs() {

        String envName = selectedEnv.getName().get();
        devColumn.setText("Devices from " + envName);
        DevEntry template = new DevEntry(null, envName);
        List<DevEntry> devs = new ArrayList();
        while (true) {
            DevEntry gotEntry;
            try {
                gotEntry = (DevEntry) space.take(template, null, timeToRead);
                if (gotEntry == null)
                    break;
                devs.add(gotEntry);
            } catch (Exception e) {
                System.out.println(e);
                break;
            }
        }
        devList.clear();
        for (DevEntry entry : devs) {
            try {
                space.write(entry, null, Lease.FOREVER);
                devList.add(new TableElement(entry.name));
            } catch (TransactionException e) {
                e.printStackTrace();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public int getGreatestDevId() {

        DevEntry template = new DevEntry();
        int greatest = 0;
        List<DevEntry> devs = new ArrayList();
        while (true) {
            DevEntry gotEntry;
            try {
                gotEntry = (DevEntry) space.take(template, null, timeToRead);
                if (gotEntry == null)
                    break;
                String stringId = gotEntry.name.substring("Dev".length());
                int id = Integer.parseInt(stringId);
                greatest = Integer.max(greatest, id);
                devs.add(gotEntry);
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
        }
        for (DevEntry entry : devs) {
            try {
                space.write(entry, null, Lease.FOREVER);
            } catch (TransactionException e) {
                e.printStackTrace();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return greatest;
    }

    @FXML
    public void addDev() {

        if (selectedEnv == null) {
            showWarning("Select an environment");
            return;
        }
        String devName = "Dev" + (getGreatestDevId() + 1);
        DevEntry devEntry = new DevEntry(devName, selectedEnv.getName().get());
        try {
            space.write(devEntry, null, Lease.FOREVER);
            devList.add(new TableElement(devEntry.name));
        } catch (TransactionException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void deleteDev() {

        if (selectedEnv == null || selectedDev == null) {
            showWarning("Select an environment and a device");
            return;
        }
        String devName = selectedDev.getName().get();
        String envName = selectedEnv.getName().get();
        int index = devList.indexOf(selectedDev);
        DevEntry template = new DevEntry(devName, envName);
        try {
            space.take(template, null, Long.MAX_VALUE);
            devList.remove(index);
        } catch (Exception e) {
            System.out.println(e);
        }
        if (devList.isEmpty())
            selectedDev = null;
    }

    @FXML
    public void changeEnv() {

        if (selectedDev == null) {
            showWarning("Select a device");
            return;
        }
        List<EnvEntry> envs = getEnvs();
        List<String> names = new ArrayList<>();
        for (EnvEntry entry : envs)
            names.add(entry.name);
        ChoiceDialog<String> dialog = new ChoiceDialog<>(null, names);
        dialog.setTitle("Environment selection");
        dialog.setHeaderText("Choose a new environment");
        dialog.setContentText("to " + selectedDev.getName().get() + ":");

        Optional<String> result = dialog.showAndWait();
        result.ifPresent(newEnv -> takeDevToNewEnv(selectedDev.getName().get(), newEnv));
    }

    private void takeDevToNewEnv(String dev, String newEnv) {

        selectedDev = null;
        DevEntry template = new DevEntry(dev, null);
        try {
            space.take(template, null, 1 * 100);
        } catch (Exception e) {
            System.out.println(e);
        }
        template = new DevEntry(dev, newEnv);
        try {
            space.write(template, null, Lease.FOREVER);
            showDevs();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Users methods

    public void showUsr() {

        if (selectedEnv == null) {
            showWarning("Select an environment");
            return;
        }
        String envName = selectedEnv.getName().get();
        usrColumn.setText("Users from " + envName);
        UsrEntry template = new UsrEntry(null, envName);
        usrList.clear();
        List<UsrEntry> users = new ArrayList();
        while (true) {
            UsrEntry gotEntry;
            try {
                gotEntry = (UsrEntry) space.take(template, null, timeToRead);
                if (gotEntry == null)
                    break;
                users.add(gotEntry);
            } catch (Exception e) {
                System.out.println(e);
                break;
            }
        }
        usrList.clear();
        for (UsrEntry entry : users) {
            try {
                space.write(entry, null, Lease.FOREVER);
                usrList.add(new TableElement(entry.name));
            } catch (TransactionException e) {
                e.printStackTrace();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void updateUsers() { showUsr(); }

    public void showWarning(String message) {

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Invalid selection");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }
}
