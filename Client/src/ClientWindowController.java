import Tuples.DevEntry;
import Tuples.MsgEntry;
import Tuples.UsrEntry;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import net.jini.core.lease.Lease;
import net.jini.core.transaction.TransactionException;
import net.jini.space.JavaSpace;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class ClientWindowController {

    @FXML TableView<TableElement> usrTable;
    @FXML TableView<TableElement> devTable;

    @FXML TableColumn usrColumn;
    @FXML TableColumn devColumn;

    @FXML TextArea chatArea;

    @FXML TextField textField;

    @FXML Label loginLabel;

    private JavaSpace space;

    private ObservableList<TableElement> devList = FXCollections.observableArrayList();
    private ObservableList<TableElement> usrList = FXCollections.observableArrayList();

    private TableElement selectedUsr;

    private Boolean listen = true;

    private String userName;
    private String envName;

    public void setSpace(JavaSpace space) {
        this.space = space;
    }

    public void setUserName(String userName) {

        this.userName = userName;
        loginLabel.setText(userName);
    }

    public void setEnvName(String envName) {
        this.envName = envName;
    }

    public void setListen(Boolean listen) { this.listen = listen; }

    public void initUI() {

        devTable.setItems(devList);
        devTable.setPlaceholder(new Label("No devices added"));
        devColumn.setText("Devices from " + envName);
        devColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<TableElement, String>,
                ObservableValue<String>>) c -> c.getValue().getName());

        usrTable.setItems(usrList);
        usrTable.setPlaceholder(new Label("No users connected"));
        usrColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<TableElement, String>,
                        ObservableValue<String>>) c -> c.getValue().getName());
        usrTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null)
                selectedUsr = newValue;
        });
        chatArea.textProperty().addListener(observable -> {
            chatArea.setScrollTop(Double.MAX_VALUE);
        });
        showDevs();
        showUsr();
        initUpdateMessages();
    }

    //Device methods

    public void showDevs() {

        DevEntry template = new DevEntry(null, envName);
        List<DevEntry> devs = new ArrayList<>();
        while (true) {
            DevEntry gotEntry;
            try {
                gotEntry = (DevEntry) space.take(template, null, 1 * 100);
                if (gotEntry == null)
                    break;
                devs.add(gotEntry);
            } catch (Exception e) {
                System.out.println(e);
                break;
            }
        }
        devList.clear();
        for (DevEntry entry : devs) {
            try {
                space.write(entry, null, Lease.FOREVER);
                devList.add(new TableElement(entry.name));
            } catch (TransactionException e) {
                e.printStackTrace();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void updateDevTable() { showDevs(); }

    //Messages methods

    @FXML
    public void writeMessage() {

        if (envName == null || selectedUsr == null || textField.getText().isEmpty()) {
            showWarning("Select a user and type a message");
            return;
        }
        UsrEntry template = new UsrEntry(selectedUsr.getName().get(), null);
        try {
            UsrEntry gotEntry = (UsrEntry) space.read(template, null, 1 * 100);
            if (gotEntry == null || userName.compareTo(gotEntry.name) == 0 ||
                envName.compareTo(gotEntry.env) != 0) {
                    selectedUsr = null;
                    showWarning("The selected user doesn't exist or belong in another environment");
                    showUsr();
                    return;
            }
            MsgEntry msg = new MsgEntry(userName, gotEntry.name, textField.getText());
            space.write(msg, null, Lease.FOREVER);
            displayMessage(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayMessage(MsgEntry msg) {

        String content = "";
        content += msg.sender + ": " + msg.content + "\n\n";
        chatArea.appendText(content);
        if (msg.sender.compareTo(userName) == 0)
            textField.setText("");
    }

    public void initUpdateMessages() {

        new Thread(() -> {
            while (listen) {
                MsgEntry template = new MsgEntry(null, userName, null);
                try {
                    MsgEntry msg = (MsgEntry) space.take(template, null, 1 * 100);
                    if (msg == null)
                        continue;
                    boolean found = false;
                    for (TableElement element : usrList)
                        if (element.getName().get().compareTo(msg.sender) == 0)
                            found = true;
                    if (!found)
                        showUsr();
                    displayMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @FXML
    public void changeEnv() {

        try {
            listen = false;
            devTable.getScene().getWindow().hide();
            removeUser();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXML/SelectEnvWindow.fxml"));
            SelectEnvController controller = new SelectEnvController();
            loader.setController(controller);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Environment Selection");
            stage.setScene(new Scene(loader.load(), 280, 150));
            controller.setSpace(space);
            controller.setUserName(userName);
            controller.initUI();
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Users methods

    public void showUsr() {

        UsrEntry template = new UsrEntry(null, envName);
        List<UsrEntry> users = new ArrayList<>();
        while (true) {
            UsrEntry gotEntry;
            try {
                gotEntry = (UsrEntry) space.take(template, null, 1 * 100);
                if (gotEntry == null)
                    break;
                users.add(gotEntry);
            } catch (Exception e) {
                System.out.println(e);
                break;
            }
        }
        usrList.clear();
        for (UsrEntry entry : users) {
            try {
                space.write(entry, null, Lease.FOREVER);
                if (entry.name.compareTo(userName) != 0)
                    usrList.add(new TableElement(entry.name));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void updateUsrTable() { showUsr(); }

    public void removeUser() {

        UsrEntry template = new UsrEntry(userName, null);
        try {
            space.take(template, null, 1 * 100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showWarning(String message) {

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Invalid selection");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }
}