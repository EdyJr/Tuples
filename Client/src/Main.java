import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import net.jini.space.JavaSpace;
import java.net.URL;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        try {
            System.out.println("Searching for JavaSpace Service...");
            Lookup finder = new Lookup(JavaSpace.class);
            JavaSpace space = (JavaSpace) finder.getService();
            if (space == null) {
                showError();
                System.exit(-1);
            }
            try {
                SelectEnvController controller = new SelectEnvController();
                controller.setSpace(space);
                URL path = getClass().getResource("FXML/SelectEnvWindow.fxml");
                FXMLLoader loader = new FXMLLoader(path);
                loader.setController(controller);
                AnchorPane root = loader.load();
                loader.setController(controller);
                controller.initUI();
                primaryStage.setTitle("Environment Selection");
                primaryStage.setScene(new Scene(root, 280, 150));
                primaryStage.show();
            } catch (Exception e) {
                System.out.println(e);
                System.exit(-1);
            }
        } catch (Exception e) {
            System.out.println(e);
            System.exit(-1);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void showError() {

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Service not found");
        alert.setContentText("The JavaSpace service wasn't found. Exiting...");
        alert.showAndWait();
    }
}