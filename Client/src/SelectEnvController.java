import Tuples.EnvEntry;
import Tuples.UsrEntry;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import net.jini.core.lease.Lease;
import net.jini.core.transaction.TransactionException;
import net.jini.space.JavaSpace;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class SelectEnvController {

    @FXML public ComboBox<String> envBox;

    private JavaSpace space;

    private String userName = "";

    public void setSpace(JavaSpace space) {
        this.space = space;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void initUI() {
        envBox.setOnMouseClicked(event -> loadEnvs());
    }

    public void loadEnvs() {

        List<EnvEntry> envs = new ArrayList<>();
        EnvEntry template = new EnvEntry();
        while (true) {
            EnvEntry gotEntry;
            try {
                gotEntry = (EnvEntry) space.take(template, null, 1 * 100);
                if (gotEntry == null)
                    break;
                envs.add(gotEntry);
            } catch (Exception e) {
                System.out.println(e);
                break;
            }
        }
        envBox.getItems().clear();
        for (EnvEntry entry : envs) {
            try {
                space.write(entry, null, Lease.FOREVER);
                envBox.getItems().add(entry.name);
            } catch (TransactionException e) {
                e.printStackTrace();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void enter() {

        try {
            if (!registerUser())
                return;
            envBox.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXML/ClientWindow.fxml"));
            ClientWindowController controller = new ClientWindowController();
            loader.setController(controller);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Client Window");
            stage.setScene(new Scene(loader.load(), 430, 400));
            controller.setSpace(space);
            controller.setEnvName(envBox.getSelectionModel().getSelectedItem());
            controller.setUserName(userName);
            controller.initUI();
            stage.show();
            stage.setOnCloseRequest(event -> {
                controller.setListen(false);
                controller.removeUser();
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean registerUser() {

        String envName = envBox.getSelectionModel().getSelectedItem();
        if (envName == null || envName.isEmpty())
            return false;
        if (userName.isEmpty())
            userName += "User" + (getGreatestUserId() + 1);

        UsrEntry template = new UsrEntry(userName, null);
        try {
            space.take(template, null, 1 * 100);
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        template = new UsrEntry(userName, envName);
        try {
            space.write(template, null, Lease.FOREVER);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public int getGreatestUserId() {

        UsrEntry template = new UsrEntry(null, null);
        int greatest = 0;
        List<UsrEntry> users = new ArrayList();
        while (true) {
            UsrEntry gotEntry;
            try {
                gotEntry = (UsrEntry) space.take(template, null, 1 * 100);
                if (gotEntry == null)
                    break;
                String stringId = gotEntry.name.substring("User".length());
                int id = Integer.parseInt(stringId);
                greatest = Integer.max(greatest, id);
                users.add(gotEntry);
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
        }
        for (UsrEntry entry : users) {
            try {
                space.write(entry, null, Lease.FOREVER);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return greatest;
    }
}
