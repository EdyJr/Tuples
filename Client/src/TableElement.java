import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TableElement {

    private final SimpleStringProperty name;

    public TableElement(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public StringProperty getName() {
        return name;
    }
}
