package Tuples;

import net.jini.core.entry.Entry;

public class DevEntry implements Entry {

    public String name, env;

    public DevEntry() {}

    public DevEntry(String name, String env) {

        this.name = name;
        this.env = env;
    }
}
