package Tuples;

import net.jini.core.entry.Entry;

public class MsgEntry implements Entry {

    public String sender, receiver, content;

    public MsgEntry() {}

    public MsgEntry(String sender, String receiver, String content) {

        this.sender = sender;
        this.receiver = receiver;
        this.content = content;
    }
}
