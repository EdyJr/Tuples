package Tuples;

import net.jini.core.entry.Entry;

public class UsrEntry implements Entry {

    public String name, env;

    public UsrEntry() {}

    public UsrEntry(String name, String env) {

        this.name = name;
        this.env = env;
    }
}